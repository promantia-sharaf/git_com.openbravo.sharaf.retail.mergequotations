/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.mergequotations.process;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class ScanOpenTransaction extends ProcessHQLQuery {
  public static final Logger log = Logger.getLogger(ScanOpenTransaction.class);

  @Override
  protected Map<String, Object> getParameterValues(JSONObject jsonsent) throws JSONException {
    try {
      OBContext.setAdminMode(true);
      Map<String, Object> paramValues = new HashMap<String, Object>();
      paramValues.put("client", jsonsent.getString("client"));
      paramValues.put("organization", jsonsent.getString("organization"));
      paramValues.put("filter", jsonsent.optJSONObject("parameters").optJSONObject("filters")
          .getString("filterText"));

      return paramValues;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    String hqlScanOpenTransaction = "select ord.id as id, ord.documentNo as documentNo, ord.businessPartner as businessPartner, "
        + " ord.custshaCustomerName as custshaCustomerName, "
        + " ord.custshaCustomerPhone as custshaCustomerPhone, "
        + " ord.custshaCustomerEmail as custshaCustomerEmail, "
        + " ord.custshaCustomerAddress as custshaCustomerAddress, "
        + " ord.custshaWebsiterefno as custshaWebsiterefno, "
        + " ord.businessPartner.cUSTSHADGInvoiceCount as bpInvoiceCount, ord.businessPartner.cUSTSHADGItemQtyCount as bpItemQtyCount "
        + " from Order as ord"
        + " where ord.documentNo= :filter"
        + " and ord.client.id= :client"
        + " and ord.organization.id= :organization and ord.documentStatus ='UE'";
    return Arrays.asList(new String[] { hqlScanOpenTransaction });
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }
}