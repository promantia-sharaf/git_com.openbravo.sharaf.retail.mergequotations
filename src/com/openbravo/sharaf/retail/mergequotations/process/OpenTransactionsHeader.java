/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.mergequotations.process;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class OpenTransactionsHeader extends ProcessHQLQuery {
  public static final Logger log = Logger.getLogger(OpenTransactionsHeader.class);

  @Override
  protected Map<String, Object> getParameterValues(JSONObject jsonsent) throws JSONException {
    Map<String, Object> paramValues = new HashMap<String, Object>();
    boolean useContains = true;
    try {
      OBContext.setAdminMode(true);
      useContains = "Y".equals(Preferences.getPreferenceValue("OBPOS_remote.order_usesContains",
          true, OBContext.getOBContext().getCurrentClient(),
          OBContext.getOBContext().getCurrentOrganization(), OBContext.getOBContext().getUser(),
          OBContext.getOBContext().getRole(), null));
      paramValues.put("client", jsonsent.getString("client"));
      paramValues.put("organization", jsonsent.getString("organization"));
      JSONObject json = jsonsent.optJSONObject("parameters").optJSONObject("filters");
      if (!json.getString("filterText").isEmpty()) {
        if (useContains) {
          paramValues.put("filter", ("%" + json.getString("filterText").trim() + "%"));
        } else {
          paramValues.put("filter", (json.getString("filterText").trim() + "%"));
        }
      }
    } catch (PropertyException e) {
      log.error("Error getting preference OBPOS_remote.receipt_usesContains " + e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return paramValues;
  }

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {

    JSONObject json = jsonsent.optJSONObject("parameters").optJSONObject("filters");
    String hqlOpenTransactions = "select ord.id as id, ord.documentNo as documentNo, ord.orderDate as orderDate, "
        + " ord.custshaCustomerName as custshaCustomerName, "
        + " ord.custshaCustomerPhone as custshaCustomerPhone, "
        + " ord.custshaCustomerEmail as custshaCustomerEmail, "
        + " ord.custshaCustomerAddress as custshaCustomerAddress, "
        + " ord.custshaWebsiterefno as custshaWebsiterefno, "
        + " ord.grandTotalAmount as totalamount, ord.businessPartner as businessPartner, "
        + " ord.businessPartner.cUSTSHADGInvoiceCount as bpInvoiceCount, ord.businessPartner.cUSTSHADGItemQtyCount as bpItemQtyCount from Order as ord "
        + " where ord.client.id = :client" + " and ord.organization.id = :organization"
        + " and ord.documentStatus = 'UE'";

    if (!json.getString("filterText").isEmpty()) {
      hqlOpenTransactions += " and ord.documentNo like :filter or upper(ord.businessPartner.name) like upper(:filter)";
    }

    hqlOpenTransactions += " order by ord.orderDate desc, ord.documentNo desc";

    return Arrays.asList(new String[] { hqlOpenTransactions });
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }
}