/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

OB.UTIL.HookManager.registerHook('OBPOS_BarcodeSearch', function (args, callbacks) {

	var orderPayments =  OB.MobileApp.model.receipt.get('payments');
	var len = orderPayments.length;
	for (i=0; i<len; i++){
		var paymentMethod = orderPayments.models[i].get('kind');
	    if ( (paymentMethod === 'CUSTSPM_payment.card_MasterCard' || paymentMethod === 'CUSTSPM_payment.giftcard' || paymentMethod === 'CUSTSPM_payment.card_VISA' || paymentMethod === 'CUSTSPM_payment.creditcard' || paymentMethod === 'CUSTSPM_payment.card_AMEX' || paymentMethod === 'CUSTSPM_payment.card_UnionPay' || paymentMethod === 'CUSTSPM_payment.yougotagift' || paymentMethod === 'GCNV_payment.creditnote') && args.dataProducts.length === 0) {
	      args.cancelOperation = true;
	      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_QtnValidation'));
	      return;
	      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
	    }
	}

  if (args.dataProducts.length === 0) {
   // 291
   var quotationdocType = args.code.substr(0,2),
         preferredQuotationDocType = (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation) && 
                OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation.includes(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey').trim()))
                ?true : false; 
    if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation) && quotationdocType === 'OO' && !preferredQuotationDocType) {
        args.cancelOperation = true;
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('SHAQUO_InvoiceTypeForQuotation', [OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation, quotationdocType])); 
    } else {
        OB.SHAQUO.findOpenTransaction(args.code, function () {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          }, function () {
            args.cancellation = true;
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        });
    }  
  } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
});