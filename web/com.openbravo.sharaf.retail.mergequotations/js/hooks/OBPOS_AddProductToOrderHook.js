/**
 * Promantia Development
 */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_AddProductToOrder', function (args, callbacks) {

    if (!OB.UTIL.isNullOrUndefined(args.productToAdd.get('digiProductType'))) {
      if (!OB.UTIL.isNullOrUndefined(args.receipt.get('HTTPRequest')) && !OB.UTIL.isNullOrUndefined(args.receipt.get('HTTPResponse')) && !OB.UTIL.isNullOrUndefined(args.receipt.get('epayResponse'))) {
        args.productToAdd.set('HTTPRequest', args.receipt.get('HTTPRequest'));
        args.productToAdd.set('HTTPResponse', args.receipt.get('HTTPResponse'));
        args.productToAdd.set('epayResponse', args.receipt.get('epayResponse'));
      }
      if (!OB.UTIL.isNullOrUndefined(args.receipt.get('digiSerialNo'))) {
        args.productToAdd.set('digiSerialNo', args.receipt.get('digiSerialNo'));
      }
    }

    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());