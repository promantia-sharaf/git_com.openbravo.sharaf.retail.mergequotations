/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo*/

enyo.kind({
  name: 'SHAQUO.UI.MenuAddQuotation',
  kind: 'OB.UI.MenuAction',
  permission: 'SHAQUO_menuAddQuotation',
  events: {
    onShowPopup: ''
  },
  i18nLabel: 'SHAQUO_LblAddQuotation',
  tap: function () {
// validation to check for few payments present in ticket before adding quotation
    var promptPopup;
    var cUSTSHAPaymentType = this.owner.owner.model.get('order').get('payments');
    var i = 0,
        len = cUSTSHAPaymentType.length;

    for (i; i < len; i++) {
      var paymentMethod = cUSTSHAPaymentType.models[i].get('kind');
      if (paymentMethod === 'CUSTSPM_payment.card_MasterCard' || paymentMethod === 'CUSTSPM_payment.giftcard' || paymentMethod === 'CUSTSPM_payment.card_VISA' || paymentMethod === 'CUSTSPM_payment.creditcard' || paymentMethod === 'CUSTSPM_payment.card_AMEX' || paymentMethod === 'CUSTSPM_payment.card_UnionPay' || paymentMethod === 'CUSTSPM_payment.yougotagift' || paymentMethod === 'GCNV_payment.creditnote') {
    	  promptPopup = true;
      }
    }

    if(!OB.UTIL.isNullOrUndefined(promptPopup) && promptPopup === true ){
    	OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_QtnValidation'));
        return;
//end of validation
    }else{
    	var me = this;
    	var connectedCallback = function () {
        if (OB.MobileApp.model.hasPermission(me.permission)) {
          me.doShowPopup({
            popup: 'SHAQUO_modalAddQuotation'
          });
        }
        };
        var notConnectedCallback = function () {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
        return;
        };
        if (this.disabled) {
        	return true;
        }
        this.inherited(arguments); // Manual dropdown menu closure
        if (!OB.MobileApp.model.get('connectedToERP')) {
        	OB.UTIL.checkOffLineConnectivity(500, connectedCallback, notConnectedCallback);
        } else {
        	connectedCallback();
        }
    }
  }
});

// Register the menu...
OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
  kind: 'SHAQUO.UI.MenuAddQuotation'
});