/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, Backbone, _, enyo */
enyo.kind({
  name: 'SHAQUO.UI.ModalOTScrollableHeader',
  kind: 'OB.UI.ScrollableTableHeader',
  events: {
    onSearchAction: '',
    onClearAction: ''
  },
  handlers: {
    onFiltered: 'searchAction'
  },
  components: [{
    style: 'padding: 10px;',
    components: [{
      style: 'display: table;',
      components: [{
        style: 'display: table-cell; width: 100%;',
        components: [{
          kind: 'OB.UI.SearchInputAutoFilter',
          name: 'filterText',
          style: 'width: 100%',
          skipAutoFilterPref: 'OBPOS_remote.order'

        }]
      }, {
        style: 'display: table-cell;',
        components: [{
          kind: 'OB.UI.SmallButton',
          classes: 'btnlink-gray btn-icon-small btn-icon-clear',
          style: 'width: 100px; margin: 0px 5px 8px 19px;',
          ontap: 'clearAction'
        }]
      }, {
        style: 'display: table-cell;',
        components: [{
          kind: 'OB.UI.SmallButton',
          classes: 'btnlink-yellow btn-icon-small btn-icon-search',
          style: 'width: 100px; margin: 0px 0px 8px 5px;',
          ontap: 'searchAction'
        }]
      }]
    }]
  }],
  disableFilterText: function (value) {
    this.$.filterText.setDisabled(value);
  },
  clearAction: function () {
    if (!this.$.filterText.disabled) {
      this.$.filterText.setValue('');
    }
    this.doClearAction();
    this.searchAction();
  },
  searchAction: function () {
    this.filters = {
      filterText: this.$.filterText.getValue()
    };
    this.doSearchAction({
      filters: this.filters
    });
    return true;
  },
  initSearchAction: function () {
    this.disableFilterText(false);
    this.clearAction();
    this.searchAction();
  }
});

/*items of collection*/
enyo.kind({
  name: 'SHAQUO.UI.ListOTsLine',
  kind: 'OB.UI.listItemButton',
  events: {
    onHideThisPopup: ''
  },
  tap: function () {
    this.inherited(arguments);
    this.doHideThisPopup();
  },
  components: [{
    name: 'line',
    style: 'line-height: 23px;',
    components: [{
      name: 'topLine'
    }, {
      style: 'color: #888888',
      name: 'bottonLine'
    }, {
      style: 'clear: both;'
    }]
  }],
  create: function () {
    this.inherited(arguments);
    this.$.topLine.setContent(this.model.get('documentNo') + ' - ' + this.model.get('businessPartner').name);
    this.$.bottonLine.setContent(this.model.get('totalamount') + ' (' + this.model.get('orderDate').substring(0, 10) + ') ');
    this.render();
  }
});

/*scrollable table (body of modal)*/
enyo.kind({
  name: 'SHAQUO.UI.ListOTs',
  classes: 'row-fluid',
  handlers: {
    onSearchAction: 'searchAction',
    onClearAction: 'clearAction'
  },
  events: {
    onShowPopup: '',
    onChangeCurrentOrder: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'prslistitemprinter',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '350px',
          renderHeader: 'SHAQUO.UI.ModalOTScrollableHeader',
          renderLine: 'SHAQUO.UI.ListOTsLine',
          renderEmpty: 'OB.UI.RenderEmpty'
        }, {
          name: 'renderLoading',
          style: 'border-bottom: 1px solid #cccccc; padding: 20px; text-align: center; font-weight: bold; font-size: 30px; color: #cccccc',
          showing: false,
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblLoading'));
          }
        }]
      }]
    }]
  }],
  clearAction: function () {
    this.prsList.reset();
    return true;
  },
  searchAction: function (inSender, inEvent) {
    var me = this,
        ordersLoaded = [],
        process = new OB.DS.Request('com.openbravo.sharaf.retail.mergequotations.process.OpenTransactionsHeader'),
        i;
    me.filters = inEvent.filters;
    this.clearAction();
    this.$.prslistitemprinter.$.tempty.hide();
    this.$.prslistitemprinter.$.tbody.hide();
    this.$.prslistitemprinter.$.tlimit.hide();
    this.$.renderLoading.show();
    var limit;
    if (!OB.MobileApp.model.hasPermission('OBPOS_remote.order', true)) {
      limit = OB.Model.Order.prototype.dataLimit;
    } else {
      limit = OB.Model.Order.prototype.remoteDataLimit ? OB.Model.Order.prototype.remoteDataLimit : OB.Model.Order.prototype.dataLimit;
    }
    if (OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true)) {
      limit = OB.DEC.abs(OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true));
      OB.Model.Order.prototype.tempDataLimit = OB.DEC.abs(OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true));
    }
    process.exec({
      filters: me.filters,
      _limit: limit + 1,
      _dateFormat: OB.Format.date
    }, function (data) {
      if (data) {
        ordersLoaded = [];
        _.each(data, function (iter) {
          me.model.get('orderList').newDynamicOrder(iter, function (order) {
            ordersLoaded.push(order);
          });
        });
        me.prsList.reset(ordersLoaded);
        me.$.renderLoading.hide();
        if (data && data.length > 0) {
          me.$.prslistitemprinter.$.tbody.show();
        } else {
          me.$.prslistitemprinter.$.tempty.show();
        }
        me.$.prslistitemprinter.getScrollArea().scrollToTop();
      } else {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_MsgErrorDropDep'));
      }
    }, function (error) {
      if (error) {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
      }
    });
    return true;
  },
  prsList: null,
  init: function (model) {
    var me = this;
    this.model = model;
    this.prsList = new Backbone.Collection();
    this.$.prslistitemprinter.setCollection(this.prsList);
    this.prsList.on('click', function (model) {
      // If quotation is already on the system do not add it
      var quotationExists = false,
          i = 0;
      if(OB.MobileApp.model.receipt.get('lines').length == 0){
          map1 = new Map();
          OB.MobileApp.model.receipt.attributes.promoadded = map1;  
       } 
      for (i; i < OB.MobileApp.model.receipt.get('lines').length; i++) {
        if (OB.MobileApp.model.receipt.get('lines').at(i).get('sHAQUOOrder') === model.get('id')) {
          quotationExists = true;
          break;
        }
      }
      if (quotationExists) {
        OB.UTIL.showLoading(false);
        OB.UTIL.showError(OB.I18N.getLabel('SHAQUO_quotationExists', [model.get('documentNo')]));
        return;
      } else {
    	  var flag = false, quoBPNameLC = model.get('businessPartner')._identifier.toLowerCase();
    	  if( quoBPNameLC.includes('anonymous customer') ){
    		  flag = true;
    	  }
    	  // OBDEV-291
    	  
        	 var quotationdocType = model.get('documentNo').substr(0,2),
             preferredQuotationDocType = (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation) && 
                    OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation.includes(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey').trim()))
                    ?true : false; 
            if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation) && quotationdocType === 'OO' && !preferredQuotationDocType) {
                OB.UTIL.showLoading(false);
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('SHAQUO_InvoiceTypeForQuotation', [OB.MobileApp.model.attributes.permissions.SHAQUO_PreferedDocTypeForQuotation, quotationdocType])); 
                return;
            } 
    // End 291
        if (OB.MobileApp.model.receipt.get('bp').id !== model.get('businessPartner').id  && !flag) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('SHAQUO_DifferentBP'), OB.I18N.getLabel('SHAQUO_WantToProceed', [model.get('documentNo'), model.get('businessPartner').name]), [{
              label: OB.I18N.getLabel('SHAQUO_Yes'),
        	  isConfirmButton: true,
        	  action: function () {
        		  //Add invoicecount and itemqty count to BP obj externally
        		  model.get('businessPartner').cUSTSHADGInvoiceCount = model.get('bpInvoiceCount');
        		  model.get('businessPartner').cUSTSHADGItemQtyCount = model.get('bpItemQtyCount');

              OB.SHAQUO.UpdateBP(OB.MobileApp.model.receipt, model.get('businessPartner'), function () {
                OB.MobileApp.model.receipt.set('generatedFromQuotation', true);
                OB.MobileApp.model.receipt.set('custshaWebsiterefno', model.get('custshaWebsiterefno'));
                OB.MobileApp.model.receipt.set('custshaCustomerName', model.get('custshaCustomerName'));
                OB.MobileApp.model.receipt.set('custshaCustomerPhone', model.get('custshaCustomerPhone'));
                OB.MobileApp.model.receipt.set('custshaCustomerEmail', model.get('custshaCustomerEmail'));
                OB.MobileApp.model.receipt.set('custshaCustomerAddress', model.get('custshaCustomerAddress'));
                OB.PROMOTION.PROMOTIONVALIDATION(model.get('id'),function(promotionflag){
                    if(promotionflag) {
                        OB.SHAQUO.LoadOrder(model.get('id'));
                    }
                 });  
              });

              var success, error;
              success  = function() {};
              error = function() {
            	  console.log('error while saving BP in local storage for DG Member validations');
              };
              OB.Dal.saveOrUpdateNew = function(model, success, error) {
            	  OB.Dal.transaction(function(tx) {
            		  OB.Dal.getInTransaction(
                  	        tx,
                  	        OB.Model[model.modelName],
                  	        model.get('id'),
                  	        function() {
                  	          OB.Dal.save(model, success, error, false, tx);
                  	        },
                  	        error,
                  	        function() {
                  	          OB.Dal.saveInTransaction(tx, model, success, error, true);
                  	        },
                  	        true
                  	      );
            	  });
              };

              var bpInfo = new OB.Model.BusinessPartner;
              bpInfo.attributes = model.get('businessPartner');
              OB.Dal.saveOrUpdateNew (bpInfo, success, error);
        	  }
          }, {
            label: OB.I18N.getLabel('SHAQUO_No'),
            action: function () {
               OB.PROMOTION.PROMOTIONVALIDATION(model.get('id'),function(promotionflag){
                if(promotionflag) {
                    OB.SHAQUO.LoadOrder(model.get('id'));
                }
              });              
            }
          }, {
            label: OB.I18N.getLabel('OBMOBC_LblCancel')
          }
          ], {
            onShowFunction: function (popup) {
              popup.$.headerCloseButton.hide();
            },
            autoDismiss: false
          });
        } else {          
            OB.MobileApp.model.receipt.set('generatedFromQuotation', true);
            OB.PROMOTION.PROMOTIONVALIDATION(model.get('id'),function(promotionflag){
                if(promotionflag) {
                    OB.SHAQUO.LoadOrder(model.get('id'));
                }
            });        
         }
      }
    }, this);
  }
});

/*Modal definiton*/
enyo.kind({
  name: 'SHAQUO.UI.ModalAddQuotation',
  kind: 'OB.UI.Modal',
  topPosition: '125px',
  i18nHeader: 'SHAQUO_OpenQuotations',
  published: {
    params: null
  },
  body: {
    kind: 'SHAQUO.UI.ListOTs'
  },
  executeOnShow: function () {
    this.$.body.$.listOTs.$.prslistitemprinter.$.theader.$.modalOTScrollableHeader.initSearchAction();
  },
  init: function (model) {
    var me = this;
    this.model = model;
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'SHAQUO.UI.ModalAddQuotation',
  name: 'SHAQUO_modalAddQuotation'
});
