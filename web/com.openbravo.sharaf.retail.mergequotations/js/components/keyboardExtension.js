/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

OB.UI.Keyboard.extend({
  keyMatcher: /^([0-9]|\.|\/|,| |%|\\|\*|\>|[a-z]|[A-Z])$/,
  writeCharacter: function (character) {
    var content = this.$.editbox.getContent();
    var contentLength = content.length;
    if (contentLength >= 150 && character !== 'del') {
      OB.UTIL.showError(OB.I18N.getLabel('SHAQUO_ErrorMaxNumber'));
      return;
    }
    if (character.match(this.keyMatcher) && this.isEnabled) {
      this.$.editbox.setContent(content + character);
    } else if (character === 'del') {
      if (contentLength > 0) {
        this.$.editbox.setContent(content.substring(0, contentLength - 1));
      }
    }
    this.$.editbox.adjustFontSize();
    if (!OB.UTIL.isIOS()) {
      document.getElementById('_focusKeeper').value = this.$.editbox.getContent();
    }
  }
});