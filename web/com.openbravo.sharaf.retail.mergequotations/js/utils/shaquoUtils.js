/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global _, enyo*/

OB = OB || {};

OB.SHAQUO = OB.SHAQUO || {};

OB.SHAQUO.UpdateBP = function (order, bp , callback) {
  OB.Dal.get(OB.Model.BusinessPartner, bp.id, function (businessPartner) {
    businessPartner.loadBPLocations(null, null, function (shipping, billing, locations) {
      businessPartner.set('locationModel', billing);
      businessPartner.set('locId', billing.get('id'));
      businessPartner.set('locName', billing.get('name'));
      businessPartner.set('postalCode', billing.get('postalCode'));
      businessPartner.set('cityName', billing.get('cityName'));
      businessPartner.set('countryName', billing.get('countryName'));
      if (shipping) {
        businessPartner.set('shipLocId', shipping.get('id'));
        businessPartner.set('shipLocName', shipping.get('name'));
        businessPartner.set('shipPostalCode', shipping.get('postalCode'));
        businessPartner.set('shipCityName', shipping.get('cityName'));
        businessPartner.set('shipCountryName', shipping.get('countryName'));
        businessPartner.set('shipRegionId', shipping.get('regionId'));
        businessPartner.set('shipCountryId', shipping.get('countryId'));
      }
      order.set('bp', businessPartner);
      OB.MobileApp.model.receipt.save(function () {
        callback();
      });
    });
  });
};

OB.SHAQUO.findOpenTransaction = function (code, callback, callbackFound) {
  var filter = {
    filterText: code
  },
      process = new OB.DS.Request('com.openbravo.sharaf.retail.mergequotations.process.ScanOpenTransaction');
  process.exec({
    filters: filter
  }, function (data) {
    if (data && data.length === 1) {
      // If quotation is already on the system do not add it
      var quotationExists = false,
          i = 0;
       if(OB.MobileApp.model.receipt.get('lines').length == 0) {
          map1 = new Map();
          OB.MobileApp.model.receipt.attributes.promoadded = map1;  
       } 
      for (i; i < OB.MobileApp.model.receipt.get('lines').length; i++) {
        if (OB.MobileApp.model.receipt.get('lines').at(i).get('sHAQUOOrder') === data[0].id) {
          quotationExists = true;
          break;
        }
      }
      if (quotationExists) {
        OB.UTIL.showLoading(false);
        OB.UTIL.showError(OB.I18N.getLabel('SHAQUO_quotationExists', [data[0].documentNo]));
        if (callbackFound && callbackFound instanceof Function) {
          callbackFound();
        }
        return;
      }
      var flag = false,
          quoBPNameLC = data[0].businessPartner._identifier.toLowerCase();
      if (quoBPNameLC.includes('anonymous customer')) {
        flag = true;
      }

      if (OB.MobileApp.model.receipt.get('bp').id !== data[0].businessPartner.id && !flag) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('SHAQUO_DifferentBP'), OB.I18N.getLabel('SHAQUO_WantToProceed', [data[0].documentNo, data[0].businessPartner.name]), [{
          label: OB.I18N.getLabel('SHAQUO_Yes'),
          isConfirmButton: true,
          action: function () {
            //Add invoicecount and itemqty count to BP obj externally
            data[0].businessPartner.cUSTSHADGInvoiceCount = data[0].bpInvoiceCount;
            data[0].businessPartner.cUSTSHADGItemQtyCount = data[0].bpItemQtyCount;
            OB.SHAQUO.UpdateBP(OB.MobileApp.model.receipt, data[0].businessPartner, function () {
              OB.MobileApp.model.receipt.set('generatedFromQuotation', true);
              OB.MobileApp.model.receipt.set('custshaWebsiterefno', data[0].custshaWebsiterefno);
              OB.MobileApp.model.receipt.set('custshaCustomerName', data[0].custshaCustomerName);
              OB.MobileApp.model.receipt.set('custshaCustomerPhone', data[0].custshaCustomerPhone);
              OB.MobileApp.model.receipt.set('custshaCustomerEmail', data[0].custshaCustomerEmail);
              OB.MobileApp.model.receipt.set('custshaCustomerAddress', data[0].custshaCustomerAddress);
              OB.PROMOTION.PROMOTIONVALIDATION(data[0].id,function(promotionflag){
              if(promotionflag) {
                OB.SHAQUO.LoadOrder(data[0].id, callbackFound);
               }
             });           
           });

            var success, error;
            success = function () {};
            error = function () {
              console.log('error while saving BP in local storage for DG Member validations');
            };
            OB.Dal.saveOrUpdateNew = function (model, success, error) {
              OB.Dal.transaction(function (tx) {
                OB.Dal.getInTransaction(
                tx, OB.Model[model.modelName], model.get('id'), function () {
                  OB.Dal.save(model, success, error, false, tx);
                }, error, function () {
                  OB.Dal.saveInTransaction(tx, model, success, error, true);
                }, true);
              });
            };

            var bpInfo = new OB.Model.BusinessPartner;
            bpInfo.attributes = data[0].businessPartner;
            OB.Dal.saveOrUpdateNew(bpInfo, success, error);
          }
        }, {
          label: OB.I18N.getLabel('SHAQUO_No'),
          isConfirmButton: true,
          action: function () {
              OB.PROMOTION.PROMOTIONVALIDATION(data[0].id,function(promotionflag){
              if(promotionflag) {
                 OB.SHAQUO.LoadOrder(data[0].id, callbackFound);
               }
             });
           }
        }, {
          label: OB.I18N.getLabel('OBMOBC_LblCancel'),
          action: function () {
            if (callbackFound && callbackFound instanceof Function) {
              callbackFound();
            }
          }
        }], {
          onShowFunction: function (popup) {
            popup.$.headerCloseButton.hide();
          },
          autoDismiss: false
        });
      } else {
            OB.MobileApp.model.receipt.set('generatedFromQuotation', true);
            OB.PROMOTION.PROMOTIONVALIDATION(data[0].id,function(promotionflag){
            if(promotionflag) {
                OB.SHAQUO.LoadOrder(data[0].id, callbackFound);
             }
          });
        }
    } else {
      if (callback && callback instanceof Function) {
        callback();
      }
    }
  }, function (error) {
    if (callback && callback instanceof Function) {
      callback();
    }
  }, null, 15000);
};

OB.SHAQUO.CreateLines = function (originalOrder, newOrder, line, callback) {
  var linesToAdd = [],
      newAddedOrderLines = [];
  var mainProductLine;
  // Called when finish inserting quotation lines 
  var finishOrderCreation = function (linesToAdd) {
      originalOrder.calculateReceipt(function () {
        originalOrder.save(function () {
          _.each(newAddedOrderLines, function (line) {
            if (!OB.UTIL.isNullOrUndefined(line.get('custdisIrType')) && !OB.UTIL.isNullOrUndefined(mainProductLine) && !OB.UTIL.isNullOrUndefined(mainProductLine.id) && !OB.UTIL.isNullOrUndefined(line.get('custdisOrderline')) && mainProductLine.id === line.get('custdisOrderline')) {
              var searhKey = line.get('custdisIrType') === 'IRA' ? 'CUSTSPM_payment.instantA' : 'CUSTSPM_payment.instantB',
                  paymentMethod = OB.CUSTDIS.Utils.getPaymentMethod(searhKey, 'WebPOS Instant Redemption ' + (line.get('custdisIrType') === 'IRA' ? 'A' : 'B'));
              if (paymentMethod) {
                // Add Instant Redemption payment
                OB.CUSTDIS.Utils.addReceiptPayment(searhKey, paymentMethod, line.get('custdisRedemptionAmount'), originalOrder, mainProductLine ? mainProductLine : line);
              }
            } else {
              mainProductLine = line;
            }
          });
          if (OB.MobileApp.model.get('lastPaneShown') === 'payment') {
            originalOrder.trigger('scan');
          }
          OB.UTIL.showLoading(false);
        });
      });
      },
      finishOrder = _.after(1, finishOrderCreation);

  // Called when finish load related services for quotation lines 
  var finishLoadRelatedServices = function () {
      _.each(linesToAdd, function (lineToAdd) {
        if (_.isArray(lineToAdd.line.relatedLines)) {
          _.each(lineToAdd.line.relatedLines, function (related) {
            var orderline = _.find(originalOrder.get('lines').models, function (l) {
              return l.get('sHAQUOOrder') === related.orderId && l.get('shaquoOriginalOrderLineId') === related.orderlineId;
            });
            if (orderline) {
              related.orderId = originalOrder.get('id');
              related.orderDocumentNo = originalOrder.get('documentNo');
              related.orderlineId = orderline.get('id');
            }
          });
        }
        originalOrder.addProductToOrder(lineToAdd.prod, lineToAdd.line.quantity, {}, lineToAdd.line, function (success, newLine) {
          newLine.unset('originalOrderLineId');
          newLine.unset('originalDocumentNo');
          newAddedOrderLines.push(newLine);
          finishOrder();
        });
      });
      },
      finishRelatedServices = _.after(1, finishLoadRelatedServices);

  // Add quotation lines to current ticket
  var createLineFunction = function (prod, callback) {
      line.sHAQUOOrder = newOrder.orderid;
      line.originalOrderLineId = line.lineId;
      line.originalDocumentNo = newOrder.documentNo;
      line.shaquoOriginalOrderLineId = line.lineId;
      line.shaquoOriginalDocumentNo = newOrder.documentNo;
      if (originalOrder.get('custsdtDocumenttypeSearchKey') === 'BP' && line.cUSTDELDeliveryCondition !== 'H' && line.cUSTDELDeliveryCondition !== 'P' && line.cUSTDELDeliveryCondition !== 'IM') {
        delete line.cUSTDELDeliveryCondition;
        delete line.cUSTDELDeliveryTime;
      } else {
        if (line.cUSTDELDeliveryTime) {
          line.cUSTDELDeliveryTime = new Date(line.cUSTDELDeliveryTime);
        }
      }
      delete line.id;
      delete line.lineId;
      delete line.deliveredQuantity;
      prod.set('standardPrice', line.priceIncludesTax ? line.unitPrice : line.baseNetUnitPrice);
      linesToAdd.push({
        line: line,
        prod: prod
      });
      originalOrder._loadRelatedServices(prod.get('productType'), prod.get('id'), prod.get('productCategory'), function (data) {
        if (data && data.hasservices) {
          line.hasRelatedServices = true;
        }
        finishRelatedServices();
      });
      callback();
      };

  OB.Dal.get(OB.Model.Product, line.id, function (product) {
    createLineFunction(product, function () {
      callback();
    });
  }, null, function () {
    new OB.DS.Request('org.openbravo.retail.posterminal.master.LoadedProduct').exec({
      productId: line.id
    }, function (data) {
      createLineFunction(OB.Dal.transform(OB.Model.Product, data[0]), function () {
        callback();
      });
    }, function () {
      OB.error('Product not found on Add Quotation');
      callback();
    });
  });
};

OB.SHAQUO.LoadOrder = function (orderid, callbackFound) {
  var processPaidReceipts = new OB.DS.Process('org.openbravo.retail.posterminal.PaidReceipts');
  OB.UTIL.showLoading(true);
  processPaidReceipts.exec({
    orderid: orderid
  }, function (data) {
    if (data && data[0]) {
      var errors = [],
          lines = [],
          isError = false;
      OB.SHAQUO.ProdAndProdCatForDocumentType(data[0], 0, lines, errors, isError, callbackFound);
    } else {
      if (callbackFound && callbackFound instanceof Function) {
        callbackFound();
      }
    }
  }, function (error) {
    OB.UTIL.showLoading(false);
    if (callbackFound && callbackFound instanceof Function) {
      callbackFound();
    }
  });
};

OB.SHAQUO.ProdAndProdCatForDocumentType = function (data, idx, lines, errors, isError, callbackFound) {
  var lineError = false,
      docTypeArr = [];
  if (idx < data.receiptLines.length) {
    var prd = data.receiptLines[idx].id,
        documentTypeId = OB.MobileApp.model.receipt.get('custsdtDocumenttype'),
        documentSK = OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey'),
        prodArr = [],
        prodCatArr = [];
    var query = "select p.* from m_product p where p.m_product_id='" + prd + "'";
    OB.Dal.queryUsingCache(OB.Model.Product, query, [], function (product) {
      if (product.models.length > 0) {
        var prod = product.models[0];
        OB.CUSTSDT.Utils.fetchProdAndProdCatForDocumentType(documentSK, documentTypeId, prodCatArr, prodArr, prod, function () {
          OB.CUSTSDT.Utils.checkProdAndProdCatForDocumentType(documentSK, prodCatArr, prodArr, prod, lineError, function () {
            if (prod.get('isError')) {
              errors.push(prod.get('_identifier'));
              isError = true;
            } else {
              lines.push(data.receiptLines[idx]);
            }
            OB.SHAQUO.ProdAndProdCatForDocumentType(data, idx + 1, lines, errors, isError, callbackFound);
          });
        });
      }
    }, null);
  } else {
    if (isError) {
      data.receiptLines.splice(0, data.receiptLines.length);
      for (var i = 0; i < lines.length; i++) {
        data.receiptLines.push(lines[i]);
      }
      OB.SHAQUO.DigitalProductCheckForEpay(data, 0, callbackFound);
      for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
        var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
        if (docs.prefix === OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey')) {
          docTypeArr.push(docs.name);
        }
      }
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), docTypeArr + ' (' + OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') + ') Document type is not allowed to sell Article(s) ' + errors);
    } else {
      OB.SHAQUO.DigitalProductCheckForEpay(data, 0, callbackFound);
    }
  }
};

OB.SHAQUO.DigitalProductCheckForEpay = function (data, idx, callbackFound) {
  OB.SHAQUO.quoData = OB.SHAQUO.quoData || {};
  OB.SHAQUO.quoData.data = data;
  OB.SHAQUO.quoData.idx = idx;
  if (idx < data.receiptLines.length) {
    var prd = data.receiptLines[idx].id;
    var query = "select p.* from m_product p where p.m_product_id='" + prd + "'";
    OB.Dal.queryUsingCache(OB.Model.Product, query, [], function (product) {
      if (product.models.length > 0 && !OB.UTIL.isNullOrUndefined(product.models[0].get('digiProductType'))) {
        var prod = product.models[0];
        OB.SHAQUO.Epay(prod, data, idx, callbackFound);
      } else {
        OB.SHAQUO.CreateLines(OB.MobileApp.model.receipt, data, data.receiptLines[idx], function () {
          if (callbackFound && callbackFound instanceof Function) {
            callbackFound();
          }
        });
      }
    }, null);
  }
};

OB.SHAQUO.Epay = function (prod, data, idx, callbackFound) {
  if (!OB.UTIL.isNullOrUndefined(prod.get('digiProductType'))) {
    var pinPrintProduct, activationProduct, receipt = OB.MobileApp.model.receipt;
    if (prod.get('digiProductType') === 'P' || prod.get('digiProductType') === 'E' || prod.get('digiProductType') === 'SP') {
      pinPrintProduct = true;
    } else if (prod.get('digiProductType') === 'A' || prod.get('digiProductType') === 'SA') {
      activationProduct = true;
    }

    // PIN print flow
    if (pinPrintProduct) {
      OB.SHAQUO.CreateLines(receipt, data, data.receiptLines[idx], function () {
        if (callbackFound && callbackFound instanceof Function) {
          callbackFound();
        }
      });
    } else if (activationProduct) {
      showEPAYSerialNoPopupForQuotation(prod, receipt, data, idx, callbackFound);
    }
  }
};

showEPAYSerialNoPopupForQuotation = function (prod, receipt, data, idx, callbackFound) {
  OB.UTIL.showLoading(false);
  receipt.set('_identifier', prod.get('_identifier'));
  OB.MobileApp.view.waterfallDown('onShowPopup', {
    popup: 'CPSE.UI.epayActivationSerialNo',
    args: {
      epaydata: receipt,
      callback: function () {
        OB.SHAQUO.CreateLines(receipt, data, data.receiptLines[idx], function () {
        });
      }
    }
  });
};
